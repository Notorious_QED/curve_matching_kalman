from firedrake import *
import numpy as np
import scipy

from shoot import GeodesicShooter
import ensemble

class EnsembleKalmanFilter():

    def __init__(self, mesh_file, shape_tag, timesteps, q1, log_dir="."):

        self.cc = 0
        self.mesh = Mesh(mesh_file)
        self.timesteps = timesteps
        self.VDGT = VectorFunctionSpace(self.mesh, "DGT", degree=1, dim=2,
            vfamily="DGT", vdegree=1)
        self.gs = GeodesicShooter(mesh_file, shape_tag, self.timesteps)
        self.q1 = q1
        self.log_dir = log_dir

        self.max_iter = 10
        self.num_landmarks = q1.shape[0]

        self.q_dim = q1.shape[1]
        self.gamma_scale = 0.1
        self.Gamma = self.gamma_scale * np.eye(self.q_dim, dtype='float')
        self.sqrt_Gamma = scipy.linalg.sqrtm(self.Gamma)
        self.sqrt_Gamma_inv = np.linalg.inv(self.sqrt_Gamma)

        self.shoot = lambda p: self.gs.shoot(p)

        # algorithm parameters
        self.alpha_0 = 1.
        self.rho = 0.9  # \rho \in (0, 1)
        self.tau = 1 / self.rho + 1e-04  # \tau > 1/\rho (!)
        self.eta = 1e-03  # noise limit

        # termination criteria for the error
        self.atol = 1e-05
        self.P_init = None

    def predict(self):
        # shoot using ensemble momenta
        self.W = ensemble.LandmarkEnsemble()
        self.W.ensemble = np.zeros(shape=(self.P.size(), self.num_landmarks, self.q_dim))
        coords = Function(self.gs.VCG)
        for j, p in enumerate(self.P.ensemble):
            q, tmpts = self.shoot(p)
            coords += q
            self.W.ensemble[j, :, :] = tmpts

        # dump average shape for inspection
        coords.assign((1. / self.W.size()) * coords)
        old_coords = self.gs.mesh.coordinates.copy(deepcopy=True)
        self.gs.update_mesh(coords)
        File("results_sequential/w_mean_{}.pvd".format(self.cc)).write(coords)
        self.gs.update_mesh(old_coords)
        self.cc += 1

        # set average observations
        self.W_mean = self.W.mean()
        return self.q1 - self.W_mean

    def correct(self):
        P_new = ensemble.MomentumEnsemble()
        Cw_alphaGamma_inv = self.compute_Cw_op()

        for i, ((p, t), w) in enumerate(zip(self.P.ensemble, self.W.ensemble)):
            g = self.gain(w, Cw_alphaGamma_inv)
            p_new = Function(self.VDGT).assign(p + g)
            P_new.append((p_new, t))

        self.P = P_new

    def gain(self, w, Cw_alphaGamma_inv):
        p_update = np.zeros(shape=(self.num_landmarks), dtype='float')
        q_update = np.zeros(shape=(self.num_landmarks, self.q_dim), dtype='float')
        for k in range(self.num_landmarks):
            q_update[k, :] = np.dot(Cw_alphaGamma_inv[k,:,:], (self.q1 - w)[k])

        return self.compute_Cp(q_update)

    def compute_Cw(self):
        W_ens = self.W.ensemble

        Cw = np.zeros(shape=(self.num_landmarks, self.q_dim, self.q_dim),
            dtype='float')
        for j in range(self.ensemble_size):
            Cw += np.einsum('ij,ik->ijk', W_ens[j] - self.W_mean,
                W_ens[j] - self.W_mean)
        Cw /= (self.ensemble_size - 1)
        print("Cw: {}".format(Cw))
        return Cw

    def compute_Cp(self, vec):
        W_ens = self.W.ensemble
        p_mean = self.P.mean_momentum()
        print("vec: {}".format(vec))
        res = Function(self.VDGT)
        for (p, _), w in zip(self.P.ensemble, self.W.ensemble):
            w_weight = 0
            for j in range(self.num_landmarks):
                diff = w - self.W_mean
                w_weight += np.dot(diff[j, :], vec[j,:])
            print("alpha: {}".format(w_weight))
            res.assign(res + (p - p_mean) * w_weight)
        res.assign(res * (1./ (self.P.size() - 1)))
        print("Cp(vec)", res.dat.data)
        return res

    def compute_Cw_op(self):
        rhs = self.rho * self.error_norm(self.q1 - self.W_mean)
        Cw = self.compute_Cw()
        k = 0
        max_iter = 40
        alpha = self.alpha_0
        while k < max_iter:

            # compute the operator of which we need the inverse
            Cw_alphaGamma_inv = np.linalg.inv(Cw + alpha * self.Gamma)

            # compute the error norm (rhs)
            w_Cw_inv = np.einsum('ijk,ij->ik', Cw_alphaGamma_inv, self.q1 - self.W_mean)
            lhs = self.gamma_norm(w_Cw_inv)
            print("\t alpha*lhs {}, \t rhs {},\t diff {}".format(alpha*lhs,
                rhs, rhs - alpha * lhs))
            if alpha * lhs >= rhs:
                return Cw_alphaGamma_inv
            else:
                alpha *= 2
                k += 1

        print("!!! alpha failed to converge in {} iterations".format(max_iter))
        exit(1)

    def run(self, P, log=True):
        self.P_init = P  # for logging only
        self.dump_parameters()

        self.ensemble_size = P.size()
        self.P = P
        err = float("-inf")
        k = 0
        while k < self.max_iter:
            print("Iter ", k+1)
            mismatch = self.predict()
            self.dump_mean()
            n_err = self.error_norm(mismatch)

            print("\t\t --> error norm: {}".format(n_err))
            if np.fabs(n_err - err) < self.atol:
                print("No improvement in residual, terminating filter")
                break
            elif n_err <= self.tau*self.eta:
                break
            else:
                self.correct()
                err = n_err
            k += 1

        return self.P, self.W_mean

    def observation_norm(self, mat, y):
        inner_norm = np.einsum("ij,kj->ki", mat, y)
        err_norm = np.sqrt(np.einsum('ij,ij->', inner_norm, inner_norm))
        return err_norm

    def error_norm(self, mismatch):
        return self.observation_norm(self.sqrt_Gamma_inv, mismatch)

    def gamma_norm(self, mismatch):
        return self.observation_norm(self.sqrt_Gamma, mismatch)

    def dump_mean(self):
        print("W_mean:\n {}".format(self.W_mean))
        print("q1:\n {}".format(self.q1))

    def dump_parameters(self):
        import os
        if not os.path.exists(self.log_dir):
            os.makedirs(self.log_dir)

        fh = open(self.log_dir + 'parameters.log', 'w')
        fh.write("max_iter: {}\n".format(self.max_iter))
        fh.write("num_landmarks: {}\n".format(self.num_landmarks))
        fh.write("Gamma: {}\n".format(self.Gamma))
        fh.write("timesteps: {}\n".format(self.gs.timesteps))
        fh.write("alpha_0: {}\n".format(self.alpha_0))
        fh.write("rho: {}\n".format(self.rho))
        fh.write("tau: {}\n".format(self.tau))
        fh.write("eta: {}\n".format(self.eta))
        fh.write("atol: {}\n".format(self.atol))
        #fh.write("q0: {}\n".format(self.q0))
        fh.write("q1: {}\n".format(self.q1))
        if self.P_init:
            fh.write("P_init: {}\n".format(self.P_init))
        fh.close()
