from firedrake import *
import utils# import *
import numpy as np
import pdb
pd = pdb.set_trace

class GeodesicShooter():

    def __init__(self, mesh_file, shape_tag, timesteps):
        # time stuff
        self.timesteps = timesteps
        self.dt = 1/timesteps

        # parameters
        self.alpha = 2
        self.num_obs = 16

        # set up mesh
        self.scale = 5  # to fit unit circle to gmsh field
        #self.mesh = mesh

        # Function spaces
        self.mesh = Mesh(mesh_file)
        self.VCG = VectorFunctionSpace(self.mesh, "CG", degree=1, dim=2,
            vfamily="CG", vdegree=1)
        self.VDGT = VectorFunctionSpace(self.mesh, "DGT", degree=1, dim=2,
            vfamily="DGT", vdegree=1)
        self.CG = FunctionSpace(self.mesh, "CG", 1)
        self.shape_tag = shape_tag
        self.orig_coords = Function(self.VCG).interpolate(SpatialCoordinate(self.mesh))

        # set up a function shape to track
        self.shape_function = utils.shape_function(self.CG, self.shape_tag)

    def write_target_pvd(self, target):
        ufl_target = to_ufl(target)
        target_fn = Function(self.CG, name="target_points")
        target_fn.interpolate(sum(map(localise, ufl_target)))
        File("target_points.pvd").write(target_fn)

    def update_mesh(self, coords=None):
        if coords is None:
            coords = self.orig_coords
        self.mesh.coordinates.assign(coords)
        self.mesh.clear_spatial_index()

    def shoot(self, pt):
        self.update_mesh()

        q = Function(self.VCG).interpolate(SpatialCoordinate(self.mesh))

        # We need to move the momentum data into a `Function` defined on the
        # space with the underlying `mesh`. We need a mesh for each ensemble
        # member
        p_main, ts = pt
        shape = (len(p_main.dat.data), 2)
        p = Function(self.VDGT, val=np.reshape(p_main.dat.data, shape))

        template_points = self.extract_points(ts)
        print("Shooting...")
        for t in range(self.timesteps):
            u = self.update_u(p, q)
            template_points += np.array(u.at(template_points, tolerance=1e-06))  * self.dt
            q += u * self.dt
            self.update_mesh(q)

        return q, template_points

    def extract_points(self, thetas):
        # get the points on the discretised shape (i.e. the "circle" for now)
        # corresponding to the parameterisation `thetas`, here (cos(t), sin(t))
        scale = 5
        return scale * np.array([(np.cos(t), np.sin(t)) for t in thetas])

    def update_u(self, p, q):
        """ Inverts a tri-Helmholtz operator onto `f`. """
        u1 = self.invert_hh(p, q, momentum=True)
        return u1
        u2 = self.invert_hh(u1, q)
        u3 = self.invert_hh(u2, q)

    def invert_hh(self, f, q, momentum=False):
        def hh_1d(v, dv):
            return v*dv + self.alpha*dot(grad(v), grad(dv))

        v, dv = TrialFunction(self.VCG), TestFunction(self.VCG)
        vx, vy = v
        dvx, dvy = dv
        w = Function(self.VCG)
        a = (hh_1d(vx, dvx) + hh_1d(vy, dvy)) * dx
        bcs = [DirichletBC(self.VCG, 0, (11, 12, 13, 14))]

        # TODO: how to restrict the Jacobian? Curl-conforming?
        if momentum:
            J = grad(q)
            Jinv = inv(J)
            pt = dot((transpose(Jinv)), f)
            L = inner(pt, dv)('+') * dS(self.shape_tag)
            #L = inner(f, dv)('+') * dS(self.shape_tag)
        else:
            L = inner(f, dv) * dx
        solver_parameters = {'mat_type': 'aij',
                             'ksp_type': 'preonly',
                             'pc_factor_mat_solver_type': 'mumps',
                             'pc_type': 'lu'}
        solve(a == L, w, bcs=bcs, solver_parameters=solver_parameters)
        return w
