from firedrake import *
from kalman import *
from ensemble import *
import numpy as np
import utils

# ENKF and shooting parameters
timesteps     = 20
num_points    = 3
ensemble_size = 3

# mesh
mesh_file = "meshes/mesh0.msh"
shape_tag = 10

# end-point condition
q1 = utils.sample_circle(num_points, scale=4, shift=np.array([2, 3]))
utils.pdump(q1, "q1")

# set up ENKF and initial ensemble
ke = EnsembleKalmanFilter(mesh_file, shape_tag, timesteps, q1)
tt = np.linspace(0, 2*np.pi, num_points+1)[:-1]
pte = MomentumEnsemble()
for i in range(ensemble_size):
    expr = as_vector((i/ensemble_size, i/ensemble_size))
    p = Function(ke.VDGT).interpolate(expr)
    pte.append((p, tt))

# run and dump
p_ens_final, w_final = ke.run(pte)
utils.pdump(ke.gs.extract_points(tt), "q0")
utils.pdump(w_final, "w_final")
