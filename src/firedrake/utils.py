from firedrake import *
import numpy as np
import pickle

def pdump(f, name):
    po = open("{}.pickle".format(name), "wb")
    pickle.dump(f, po)
    po.close()

def pload(name):
    po = open("{}.pickle".format(name), "rb")
    f = pickle.load(po)
    po.close()
    return f

def shape_tag_indices(mesh, shape_tag):
    DGT = FunctionSpace(mesh, "CG", degree=1)
    ct, dct = TrialFunction(DGT), TestFunction(DGT)
    shape = Function(DGT)
    solve((ct*dct)('+')*dS + ct*dct*ds + ct*dct*dx == dct('+')*dS(shape_tag), shape)
    print(shape.dat.data.shape)
    return np.nonzero(shape.dat.data)[0]

def shape_tag_indices2(mesh, shape_tag):
    DGT = FunctionSpace(mesh, "DGT", degree=0)
    ct, dct = TrialFunction(DGT), TestFunction(DGT)
    shape = Function(DGT)
    solve((ct*dct)('+')*dS + ct*dct*ds == dct('+')*dS(shape_tag), shape)
    print(shape.dat.data.shape)
    return np.nonzero(shape.dat.data)[0]

def shape_tag_indices3(mesh, shape_tag):
    #DGT = FunctionSpace(mesh, "CG", degree=1)
    DGT = VectorFunctionSpace(mesh, "CG", degree=1, dim=2, vfamily="CG", vdegree=1)
    ct, dct = TrialFunction(DGT), TestFunction(DGT)
    shape = Function(DGT)
    #solve((ct*dct)('+')*dS + ct*dct*ds == dct('+')*dS(shape_tag), shape)
    solve(dot(ct, dct)('+')*dS + dot(ct, dct)*ds
        == dot(as_vector((1, 1)), dct('+'))*dS(shape_tag), shape)
    print(shape.dat.data.shape)
    return np.nonzero(shape.dat.data)[0]

def shape_function(CG, mesh_tag):
    dM = dS(mesh_tag)
    ct, dct = TrialFunction(CG), TestFunction(CG)
    shape = Function(CG, name="shape_function")
    solve((ct*dct)('+')*dS + ct*dct*ds + ct*dct*dx == dct('+')*dM, shape)
    File("shape.pvd").write(shape)
    return shape

def shape_normal(mesh, mesh_tag):
    n = FacetNormal(mesh)
    VDGT0 = VectorFunctionSpace(mesh, "DGT", degree=0, dim=2, vfamily="DGT",
        vdegree=0)
    N, dN = TrialFunction(VDGT0), TestFunction(VDGT0)
    shape_normal = Function(VDGT0, name="shape_normal")
    solve(dot(N,dN)('-')*dS + dot(N,dN)*ds == dot(n,dN)('-')*dS(mesh_tag),
        shape_normal)
    return shape_normal

def sample_circle(num_obs, scale=1, shift=0):
    thetas = np.linspace(0, 2*np.pi, num=num_obs+1)[:-1]
    positions = scale * np.array([[cos(x), sin(x)] for x in thetas]) + shift
    return positions

def plot_target(lms, tgt):
    import matplotlib.pyplot as plt
    plt.plot(tgt[:,0], tgt[:,1], 'ro-')
    plt.plot(lms[:,0], lms[:,1], 'g*-')
    plt.show()
