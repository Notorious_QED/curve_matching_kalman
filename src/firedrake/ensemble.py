import numpy as np
from firedrake import *

class Ensemble():

    def __init__(self):
        self.dim = 2
        self.ensemble = []

    def append(self, ne):
        self.ensemble.append(ne)

    def size(self):
        return len(self.ensemble)


class MomentumEnsemble(Ensemble):

    def __init__(self):
        super().__init__()

    def mean(self):
        return (self.mean_momenta(), self.mean_thetas())

    def mean_thetas(self):
        if self.size() < 1:
            print("Cannot take mean of empty ensemble")
            exit(1)
        return np.mean(self.thetas(), axis=0)

    def mean_momentum(self):
        if self.size() < 1:
            print("Cannot take mean of empty ensemble")
            exit(1)
        fs = self.ensemble[0][0].function_space()
        return Function(fs).assign(sum(self.momenta()) * (1. / self.size()))

    def momenta(self):
        return [e[0] for e in self.ensemble]

    def thetas(self):
        return [e[1] for e in self.ensemble]


class LandmarkEnsemble(Ensemble):

    def __init__(self):
        super().__init__()

    def mean(self):
        if self.size() < 1:
            print("Cannot take mean of empty ensemble")
            exit(1)

        return np.mean(self.ensemble, axis=0)

