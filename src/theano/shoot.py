import theano
from theano import function
import theano.tensor as T
from lib import *

log_dir = './'
import os
if not os.path.exists(log_dir):
    os.makedirs(log_dir)

def shoot(p0, q0, test_name, timesteps=100, plot=False):
    # landmark parameters
    DIM = 2 # dimension of the image
    SIGMA = theano.shared(np.array(.7).astype(theano.config.floatX)) # radius of the landmark

    num_landmarks = q0.shape[0]
    N = theano.shared(num_landmarks) # number of landmarks

    # timestepping
    Tend = 1 # final time
    n_steps = theano.shared(timesteps) # number of timesteps
    dt = Tend/n_steps # timesteps

    # general kernel for any sigma
    def Ksigma(q1, q2, sigma):
        r_sq = T.sqr(q1.dimshuffle(0, 'x', 1) - q2.dimshuffle('x', 0, 1)).sum(2)
        return T.exp( - r_sq / (2.*sigma**2) )

    # kernel function for the landmarks
    def Ker(q1, q2):
        return Ksigma(q1, q2, SIGMA)

    def H(q,p):
        return 0.5*T.tensordot(p, T.tensordot(Ker(q, q), p, [[1], [0]]), [[0, 1],
            [0, 1]])

    # compile the previous functions
    pe = T.matrix('p')
    qe = T.matrix('q')
    qe1 = T.matrix('q')
    qe2 = T.matrix('q')
    sigma = T.scalar()

    Kf = function([qe1, qe2], Ker(qe1, qe2))
    Ksigmaf = function([qe1, qe2, sigma], Ksigma(qe1, qe2, sigma))
    Hf = function([qe, pe], H(qe, pe))

    # compute gradients
    dq = lambda q,p: T.grad(H(q, p), p)
    dp = lambda q,p: -T.grad(H(q, p), q)
    dqf = function([qe, pe], dq(qe, pe))
    dpf = function([qe, pe], dp(qe, pe))

    # create the initial condition array
    q0 = q0.astype(theano.config.floatX)
    p0 = p0.astype(theano.config.floatX)
    x0 = np.array([q0, p0]).astype(theano.config.floatX)

    # ode to solve (Hamiltonian system)
    def ode_f(x):

        dqt = dq(x[0], x[1])
        dpt = dp(x[0], x[1])

        return T.stack((dqt, dpt))

    # Forward Euler scheme
    def euler(x,dt):
        return x + dt*ode_f(x)

    x = T.tensor3('x')

    # create loop symbolic loop
    (cout, updates) = theano.scan(fn=euler,
                                    outputs_info=[x],
                                    non_sequences=[dt],
                                    n_steps=n_steps)

    # compile it
    simf = function(inputs=[x],
                    outputs=cout,
                    updates=updates)

    xs = simf(np.array([q0, p0]).astype(theano.config.floatX))
    if plot:
        plot_q(x0, xs, num_landmarks, log_dir + test_name)
    return xs[-1, 0, :, :]
