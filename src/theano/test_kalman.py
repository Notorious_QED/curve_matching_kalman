from kalman import *
from shoot import *


test_name = "mytest"
ensemble_size = 3
num_landmarks = 6
dim = 2


# 1) define q0 and q1, the latter by shooting from the distribution
q0 = sample_circle(num_landmarks, scale=1, shift=0)
q1 = np.zeros(shape=(num_landmarks, dim))
for i in range(ensemble_size):
    p = np.random.normal(size=(num_landmarks, dim)) * 2
    q1 += shoot(p, q0, test_name, timesteps=20, plot=True)
q1 /= ensemble_size
q1 = sample_circle(num_landmarks, scale=1, shift=np.array([1, 1]))

# 2) sample a random initial momentum (from the same distribution!)
pe = Ensemble()
for i in range(ensemble_size):
    p0 = np.random.uniform(size=(num_landmarks, dim)) * 1
    pe.append(p0)

print("q0: ", q0, "\n")
print("q1: ", q1, "\n")

# 3) set up and run Kalman filter
ke = KalmanEnsemble(ensemble_size, q0, q1, test_name)
p_result = ke.run(pe)
ke.dump_parameters()

# 4) use the momentum we found to compute the mean observation
W = Ensemble()
for e in p_result.ensemble:
    W.append(ke.shoot(e))
w = W.mean()

# 5) dump to file
import pickle

po = open("p_result.pickle", "wb")
pickle.dump(p_result, po)
po.close()

po = open("w_mean.pickle", "wb")
pickle.dump(w, po)
po.close()
