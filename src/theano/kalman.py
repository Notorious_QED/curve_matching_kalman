from shoot import *
import numpy as np
import scipy
import pdb
pd = pdb.set_trace


class Ensemble():

    def __init__(self):
        self.ensemble = []

    def mean(self):
        if len(self.ensemble) < 1:
            print("Cannot take mean of empty ensemble")
            exit(1)

        res = np.zeros(shape=np.shape(self.ensemble[0]), dtype='float')
        for e in self.ensemble:
            res += e
        return res / len(self.ensemble)

    def append(self, ne):
        self.ensemble.append(ne)


class KalmanEnsemble():

    def __init__(self, ensemble_size, q0, q1, test_name, logdir='./'):
        self.ensemble_size = ensemble_size
        self.q0 = q0
        self.q1 = q1
        self.test_name = test_name

        self.q_dim = q0.shape[1]
        self.p_dim = q0.shape[1]

        self.max_iter = 10**5
        self.num_landmarks = q0.shape[0]

        self.tolerance = 1e-04
        self.Gamma = np.eye(self.q_dim, dtype='float')
        self.sqrt_Gamma = scipy.linalg.sqrtm(self.Gamma)
        self.timesteps = 20
        self.shoot = lambda p0: shoot(p0, self.q0, test_name,
            timesteps=self.timesteps, plot=True)

        # algorithm parameters
        self.alpha_0 = 1.
        self.rho = 1  # \rho \in (0, 1)
        self.tau = 1 / self.rho + 1e-04  # \tau > 1/\rho
        self.eta = 1e-03  # noise limit

        # termination criteria for the error
        self.atol = 1e-05
        self.P_init = None

    def predict(self):
        nens = Ensemble()
        for e in self.P.ensemble:
            nens.append(self.shoot(e))
        self.W = nens

    def correct(self):
        P_mean = self.P.mean()
        W_mean = self.W.mean()

        P_new = Ensemble()

        # compute atomic components of gain
        Cw_alphaGamma_inv = self.compute_Cw_op()
        Cp = self.compute_Cp()

        for i, (p, w) in enumerate(zip(self.P.ensemble, self.W.ensemble)):
            P_new.append(p + self.gain(w, Cp, Cw_alphaGamma_inv))

        self.P = P_new

    def gain(self, w, Cp, Cw_alphaGamma_inv):

        p_update = np.zeros(shape=(self.num_landmarks, self.p_dim), dtype='float')
        for k in range(self.num_landmarks):
            q_update = np.dot(Cw_alphaGamma_inv[k,:,:], (self.q1 - w)[k])
            p_update[k] = np.dot(Cp[k,:,:], q_update)

        return p_update

    def compute_Cw(self):
        W_ens = self.W.ensemble
        W_mean = self.W.mean()

        Cw = np.zeros(shape=(self.num_landmarks, self.q_dim, self.q_dim), dtype='float')
        for j in range(self.ensemble_size):
            Cw += np.einsum('ij,ik->ijk', W_ens[j] - W_mean, W_ens[j] - W_mean)
        Cw /= (self.ensemble_size - 1)
        return Cw

    def compute_Cp(self):
        W_ens = self.W.ensemble
        W_mean = self.W.mean()

        P_ens = self.P.ensemble
        P_mean = self.P.mean()

        Cp = np.zeros(shape=(self.num_landmarks, self.p_dim, self.q_dim), dtype='float')
        for j in range(self.ensemble_size):
            Cp += np.einsum('ij,ik->ijk', P_ens[j] - P_mean, W_ens[j] - W_mean)
        Cp /= (self.ensemble_size - 1)

        return Cp

    def compute_Cw_op(self):
        lhs = self.rho * self.error_norm(self.q1 - self.W.mean())
        Cw = self.compute_Cw()

        k = 0
        max_iter = 10**3
        alpha = self.alpha_0
        while k < max_iter:

            # compute the operator of which we need the inverse
            Cw_alphaGamma_inv = np.linalg.inv(Cw + alpha * self.Gamma)

            # compute the error norm (rhs)
            w_Cw_inv = np.einsum('ijk,ij->ik', Cw_alphaGamma_inv, self.q1 - self.W.mean())
            rhs = self.error_norm(w_Cw_inv)
            if alpha * rhs >= lhs:
                # we only need the operator, alpha is in there somewhere
                return Cw_alphaGamma_inv
            else:
                alpha *= 2
                k += 1

        print("!!! alpha failed to converge in {} iterations".format(max_iter))
        exit(1)

    def error_norm(self, x):
        # assumes we use an \ell^2 norm of `\sqrt(Gamma)(mismatch)`
        inner_norm = np.einsum("ij,kj->ki", self.sqrt_Gamma, x)
        err_norm = np.sqrt(np.einsum('ij,ij->', inner_norm, inner_norm))
        return err_norm

    def run(self, P):
        k = 0
        self.P_init = P  # for logging
        self.P = P
        err = float("-inf")
        while k < self.max_iter:
            print("Iter ", k)
            self.predict()
            self.dump_mean()
            n_err = self.error_norm(self.q1 - self.W.mean())
            print("\t\t --> error norm: {}".format(n_err))
            if np.fabs(n_err-err) < self.atol:
                print("No improvement in residual, terminating filter")
                break
            elif n_err <= self.tau*self.eta:
                break
            else:
                self.correct()
                err = n_err
            k += 1
        return self.P

    def dump_mean(self):
        print("W_mean: {}".format(self.W.mean()))

    def dump_parameters(self):
        import os
        if not os.path.exists(log_dir):
            os.makedirs(log_dir)

        fh = open(log_dir + 'parameters.log', 'w')
        fh.write("test_name: {}\n".format(self.test_name))
        fh.write("max_iter: {}\n".format(self.max_iter))
        fh.write("num_landmarks: {}\n".format(self.num_landmarks))
        fh.write("tolerance: {}\n".format(self.tolerance))
        fh.write("Gamma: {}\n".format(self.Gamma))
        fh.write("timesteps: {}\n".format(self.timesteps))
        fh.write("alpha_0: {}\n".format(self.alpha_0))
        fh.write("rho: {}\n".format(self.rho))
        fh.write("tau: {}\n".format(self.tau))
        fh.write("eta: {}\n".format(self.eta))
        fh.write("atol: {}\n".format(self.atol))
        fh.write("q0: {}\n".format(self.q0))
        fh.write("q1: {}\n".format(self.q1))
        if self.P_init:
            fh.write("P_init: {}\n".format(self.P_init))
        fh.close()
